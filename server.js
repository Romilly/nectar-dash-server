const express = require('express');
const mysql = require('mysql');

// Connect to Romilly's AWS MariaDB instance
// Create connection
const db = mysql.createConnection({
  host: 'database-1.cubsr7yrhfxd.us-east-2.rds.amazonaws.com',
  user: 'guest',
  password: 'password',
  database: 'nectar',
});

// Connect
db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log('MySql Connected via AWS...');
});

// Create express server
const app = express();

// Get weekly control values with 'product' search parameter (Offer | Brand | Aisle)
app.get('/getweekly/control/:id', (req, res) => {
  let sql = `SELECT WEEK_COMMENCING, CONTROL FROM weekly WHERE product LIKE '${req.params.id}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      res.status(404).send('Server Error');
    }
    console.log(result);
    res.send(result);
  });
});

// Get weekly control values with 'product' search parameter (Offer | Brand | Aisle)
app.get('/getweekly/exposed/:id', (req, res) => {
  let sql = `SELECT WEEK_COMMENCING, EXPOSED FROM weekly WHERE product LIKE '${req.params.id}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      res.status(404).send('Server Error');
    }
    console.log(result);
    res.send(result);
  });
});

// Get top line values with 'metric' search parameter (Spend | Units | Visits | Total_custs)
app.get('/gettop/:id', (req, res) => {
  let sql = `SELECT product, exposed, control, uplift, pct_uplift FROM top WHERE metric LIKE '${req.params.id}'`;
  let query = db.query(sql, (err, result) => {
    if (err) {
      res.status(404).send('Server Error');
    }
    console.log(result);
    res.send(result);
  });
});

app.listen('5000', () => {
  console.log('Server started on port 5000');
});
